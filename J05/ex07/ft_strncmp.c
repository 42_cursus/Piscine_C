/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strncmp.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 18:47:18 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/06 13:57:27 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_strncmp(char *s1, char *s2, unsigned int n)
{
	unsigned int	i;
	int				compare;

	i = 0;
	while (i < n)
	{
		compare = s1[i] - s2[i];
		if (s1[i] == '\0' || s2[i] == '\0' || s1[i] != s2[i])
			return (compare);
		else
			i++;
	}
	return (0);
}
