/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcapitalize.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 21:13:04 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/05 21:27:57 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

char	*ft_strcapitalize(char *str)
{
	int i;
	int alphanumbefore;

	i = 0;
	alphanumbefore = 0;
	while (str[i] != '\0')
	{
		if ((str[i] >= '0' && str[i] <= '9')
				|| (str[i] >= 'A' && str[i] <= 'Z')
				|| (str[i] >= 'a' && str[i] <= 'z'))
		{
			if (!alphanumbefore && (str[i] >= 'a' && str[i] <= 'z'))
				str[i] -= 32;
			else if (alphanumbefore && (str[i] >= 'A' && str[i] <= 'Z'))
				str[i] += 32;
			alphanumbefore = 1;
		}
		else
			alphanumbefore = 0;
		i++;
	}
	return (str);
}
