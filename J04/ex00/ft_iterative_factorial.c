/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_iterative_factorial.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/03 09:38:44 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/03 09:57:42 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_iterative_factorial(int nb)
{
	int i;
	int produit;

	if (nb >= 13 || nb < 0)
		return (0);
	i = 1;
	produit = 1;
	while (i <= nb)
	{
		produit *= i;
		i++;
	}
	return (produit);
}
