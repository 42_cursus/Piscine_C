/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   btree_level_count.c                                :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/16 15:23:29 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/16 15:30:06 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "ft_btree.h"

int	btree_level_count(t_btree *root)
{
	if (root)
	{
		if (btree_level_count(root->left) > btree_level_count(root->right))
			return (1 + btree_level_count(root->left));
		else
			return (1 + btree_level_count(root->righ));
	}
	return (0);
}
