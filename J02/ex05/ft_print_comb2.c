/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_print_comb2.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: tbenetre <tbenetre@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/08/31 22:31:16 by tbenetre          #+#    #+#             */
/*   Updated: 2016/09/01 12:21:43 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	ft_putnbr(int nbr)
{
	if (nbr >= 10)
	{
		ft_putnbr(nbr / 10);
		ft_putnbr(nbr % 10);
	}
	else
		ft_putchar(nbr + '0');
}

void	ft_print_couple(int c1, int c2)
{
	if (c1 < 10)
		ft_putchar('0');
	ft_putnbr(c1);
	ft_putchar(' ');
	if (c2 < 10)
		ft_putchar('0');
	ft_putnbr(c2);
}

void	ft_print_comb2(void)
{
	int nb_left;
	int nb_right;

	nb_left = 0;
	nb_right = 1;
	while (nb_right <= 100)
	{
		if (nb_right == 100 && nb_left <= 98)
		{
			nb_left++;
			nb_right = nb_left + 1;
			continue;
		}
		else if (nb_left > 98)
			break ;
		ft_print_couple(nb_left, nb_right);
		if (nb_left + nb_right != 98 + 99)
			ft_putchar(',');
		if (nb_left + nb_right != 98 + 99)
			ft_putchar(' ');
		nb_right++;
	}
}
