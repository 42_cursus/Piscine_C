/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_list_clear.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 14:09:44 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/13 17:01:25 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>
#include "ft_list.h"

void	ft_list_clear(t_list **begin_list)
{
	t_list *list;
	t_list *nlist;

	list = *begin_list;
	while (nlist->next)
	{
		nlist = list->next;
		free(list->data);
		free(list);
	}
	begin_list = NULL;
}
