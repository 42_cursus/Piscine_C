/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_sort_params.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/06 21:04:13 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/07 12:51:44 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int		ft_putchar(char c);

void	ft_print_params(char *str)
{
	int i;

	i = 0;
	while (str[i] != '\0')
	{
		ft_putchar(str[i]);
		i++;
	}
	ft_putchar('\n');
}

int		ft_strcmp(char *s1, char *s2)
{
	int i;
	int compare;

	i = 0;
	compare = 0;
	while (1 == 1)
	{
		compare = s1[i] - s2[i];
		if (s1[i] == '\0' && s2[i] == '\0')
			return (compare);
		else if (s1[i] == s2[i])
			i++;
		else
			return (compare);
	}
}

void	ft_sort_params(char **argv, int argc)
{
	int		i;
	int		j;
	char	*temp;

	i = 2;
	while (i < argc)
	{
		j = i;
		while (j > 1)
		{
			if (ft_strcmp(*(argv + j), *(argv + j - 1)) < 0)
			{
				temp = *(argv + j);
				*(argv + j) = *(argv + j - 1);
				*(argv + j - 1) = temp;
			}
			j--;
		}
		i++;
	}
}

int		main(int argc, char **argv)
{
	int i;

	if (argc > 2)
	{
		ft_sort_params(argv, argc);
		i = 1;
		while (i < argc)
		{
			ft_print_params(argv[i]);
			i++;
		}
	}
	return (0);
}
