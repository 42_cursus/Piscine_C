/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_strcmp.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/05 12:44:13 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/05 17:05:04 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

int	ft_strcmp(char *s1, char *s2)
{
	int i;
	int compare;

	i = 0;
	compare = 0;
	while (1 == 1)
	{
		compare = s1[i] - s2[i];
		if (s1[i] == '\0' && s2[i] == '\0')
			return (compare);
		else if (s1[i] == s2[i])
			i++;
		else
			return (compare);
	}
}
