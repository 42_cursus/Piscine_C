/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_concat_params.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/07 21:52:25 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/08 13:27:59 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int		ft_get_maxtrix_size(int argc, char **argv)
{
	int i;
	int j;
	int size;

	i = 1;
	size = 0;
	while (i < argc)
	{
		j = 0;
		while (argv[i][j])
		{
			size++;
			j++;
		}
		i++;
	}
	return (size);
}

char	*ft_concat_params(int argc, char **argv)
{
	int		i;
	int		j;
	int		size;
	char	*result;

	size = ft_get_maxtrix_size(argc, argv) + argc;
	result = (char*)malloc(sizeof(char*) * (size + 1));
	i = 1;
	j = 0;
	size = 0;
	while (i < argc)
	{
		while (argv[i][j])
			result[size++] = argv[i][j++];
		j = 0;
		i++;
		if (i < argc)
			result[size++] = '\n';
	}
	result[size] = '\0';
	return (result);
}
