/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_range.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/07 19:14:46 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/07 20:45:57 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <stdlib.h>

int	*ft_range(int min, int max)
{
	int i;
	int *range;

	i = 0;
	if (min >= max)
		return (0);
	range = (int*)malloc(sizeof(int) * (max - min + 1));
	while (min + i < max)
	{
		range[i] = min + i;
		i++;
	}
	range[i] = 0;
	return (range);
}
