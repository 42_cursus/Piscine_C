/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_display_file.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 20:43:25 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/15 20:44:37 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include "../includes/ft_printer.h"

#define BUFFER_SIZE 30

void	ft_wrong(char *str)
{
	ft_putstr("cat: ", 2);
	ft_putstr(str, 2);
	if (errno == 13)
		ft_putstr(": Permission denied\n", 2);
	else
		ft_putstr(": No such file or directory\n", 2);
}

void	ft_cat_loop(void)
{
	int		ret;
	char	buffer[BUFFER_SIZE + 1];

	while ((ret = read(0, buffer, BUFFER_SIZE)))
	{
		buffer[ret] = '\0';
		write(1, buffer, ret);
	}
}

void	ft_cat(int argc, char **argv)
{
	int		fd;
	int		ret;
	int		i;
	char	buffer[BUFFER_SIZE + 1];

	i = 0;
	while (i++ < argc - 1)
	{
		fd = open(argv[i], O_RDONLY);
		if (fd == -1)
		{
			ft_wrong(argv[i]);
			close(fd);
			continue ;
		}
		while ((ret = read(fd, buffer, BUFFER_SIZE)))
		{
			buffer[ret] = '\0';
			write(1, buffer, ret);
		}
		close(fd);
	}
}

int		main(int argc, char **argv)
{
	if (argc == 1)
	{
		ft_cat_loop();
		return (0);
	}
	ft_cat(argc, argv);
	return (errno);
}
