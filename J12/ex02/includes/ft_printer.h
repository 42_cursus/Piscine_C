/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_printer.h                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/14 20:55:15 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/15 18:15:17 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FT_PRINTER_H
# define FT_PRINTER_H
# define FILE_ERROR ": No such file or directory\n"
# define OFFSET_ERROR "tail: illegal offset -- "

void	ft_putchar(char c, int flag);
void	ft_putstr(char *str, int flag);
void	ft_putnbr(int nbr, int flag);

#endif
