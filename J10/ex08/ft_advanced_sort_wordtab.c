/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_advanced_sort_wordtab.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: adelhom <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2016/09/13 21:15:25 by adelhom           #+#    #+#             */
/*   Updated: 2016/09/13 21:17:19 by adelhom          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

void	ft_advanced_sort_wordtab(char **tab, int (*cmp)(char *, char *))
{
	int		i;
	int		j;
	char	*temp;

	i = 2;
	while (tab[i])
	{
		j = i;
		while (j > 1)
		{
			if ((*cmp)(*(tab + j), *(tab + j - 1)) < 0)
			{
				temp = *(tab + j);
				*(tab + j) = *(tab + j - 1);
				*(tab + j - 1) = temp;
			}
			j--;
		}
		i++;
	}
}
